package com.mowitnow.service;

import com.mowitnow.command.Command;
import com.mowitnow.model.coordinate.Direction;
import com.mowitnow.model.lawn.Lawn;
import com.mowitnow.model.mower.Mower;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CommandExecutorTest {

    List<String> input = Arrays.asList("5 5","1 2 NORTH","GAGAGAGAA","3 3 EAST","AADAADADDA");

    CommandExecutor    executor    = null;

    @Before public void setUp() {
        CommandInterpreter interpreter = new CommandInterpreter();
        executor = new CommandExecutor();
        executor.setInterpreter(interpreter);
    }

    @Test public void
    should_execute_commands() throws IOException{

        Lawn lawn = null;
        executor.execute(lawn,input);

        assertThat(lawn.getMowers())
            .isNotEmpty()
            .hasSize(2);

        assertThat(lawn.getMowers().get(0).getPosition().getX()).isEqualTo(1);
        assertThat(lawn.getMowers().get(0).getPosition().getY()).isEqualTo(3);
        assertThat(lawn.getMowers().get(0).getDirection()).isEqualTo(Direction.NORTH);

        assertThat(lawn.getMowers().get(1).getPosition().getX()).isEqualTo(5);
        assertThat(lawn.getMowers().get(1).getPosition().getY()).isEqualTo(1);
        assertThat(lawn.getMowers().get(1).getDirection()).isEqualTo(Direction.EAST);

    }
}