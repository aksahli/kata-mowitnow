package com.mowitnow.service;

import com.mowitnow.command.Command;
import com.mowitnow.model.coordinate.Direction;
import com.mowitnow.model.lawn.Lawn;
import com.mowitnow.model.mower.Mower;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.assertj.core.api.Assertions.assertThat;

public class CommandInterpreterTest {

    CommandInterpreter interpreter = new CommandInterpreter();

    @Test public void
    should_throw_exception_when_lawn_data_is_null() {
        String lawnInput = null;
        try {
            interpreter.getLawn(lawnInput);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Invalid lawn data");
        }
    }

    @Test public void
    should_throw_exception_when_lawn_data_is_invalid() {
        String lawnInput = "INVALID";
        try {
            interpreter.getLawn(lawnInput);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Invalid lawn data");
        }
    }

    @Test public void
    should_create_a_lawn_from_input() {
        String lawnInput = "5 5";
        Lawn lawn = interpreter.getLawn(lawnInput);
        assertThat(lawn.getWidth()).isEqualTo(5);
        assertThat(lawn.getHeight()).isEqualTo(5);
    }

    @Test public void
    should_throw_exception_when_mower_data_is_null() {
        String mowerInput = null;
        try {
            interpreter.getMower(mowerInput);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Invalid mower data");
        }
    }

    @Test public void
    should_throw_exception_when_mower_data_is_invalid() {
        String mowerInput = "INVALID";
        try {
            interpreter.getMower(mowerInput);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Invalid mower data");
        }
    }

    @Test public void
    should_create_a_mower_from_input() {
        String mowerInput = "1 1 NORTH";
        Mower mower = interpreter.getMower(mowerInput);
        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(1);
        assertThat(mower.getDirection()).isEqualTo(Direction.NORTH);
    }

    @Test public void
    should_throw_exception_when_command_input_is_null() {
        String commandInput = null;
        try {
            interpreter.getCommand(commandInput);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Invalid command input");
        }
    }

    @Test public void
    should_throw_exception_when_command_input_is_invalid() {
        String commandInput = "INVALID";
        try {
            interpreter.getCommand(commandInput);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Invalid command input");
        }
    }

    @Test public void
        should_create_a_command_from_input() {
        String commandInput = "AAGGDD";
        List<Command> commands = interpreter.getCommand(commandInput);
        assertThat(commands)
                .isNotNull()
                .hasSize(6);

    }
}