package com.mowitnow.model.mower;

import com.mowitnow.model.action.Rotate;
import org.junit.Test;

import static com.mowitnow.model.action.Rotate.RIGHT;
import static com.mowitnow.model.coordinate.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;

public class MowerTest {

    @Test public void
    should_create_a_mower_with_the_default_position_and_direction() {
        Mower mower = new Mower();
        assertThat(mower).isNotNull();
        assertThat(mower.getPosition()).isNotNull();
        assertThat(mower.getPosition().getX()).isZero();
        assertThat(mower.getPosition().getY()).isZero();
        assertThat(mower.getDirection()).isEqualTo(NORTH);
    }

    @Test public void
    should_create_a_mower_with_specified_values() {
        Mower mower = new Mower(3,7, SOUTH);
        assertThat(mower).isNotNull();
        assertThat(mower.getPosition()).isNotNull();
        assertThat(mower.getPosition().getX()).isEqualTo(3);
        assertThat(mower.getPosition().getY()).isEqualTo(7);
        assertThat(mower.getDirection()).isEqualTo(SOUTH);
    }

    @Test public void
    should_rotate_mower_to_east_when_right() {
        Mower mower = new Mower(2,4,NORTH);
        mower.rotate(RIGHT);
        assertThat(mower.getDirection()).isEqualTo(EAST);
    }

    @Test public void
    should_rotate_mower_to_west_when_left() {
        Mower mower = new Mower(2,4,NORTH);
        mower.rotate(Rotate.LEFT);
        assertThat(mower.getDirection()).isEqualTo(WEST);
    }

    @Test public void
    should_increase_mower_y_position() {
        Mower mower = new Mower(2,2,NORTH);
        mower.setLimits(5,5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(2);
        assertThat(mower.getPosition().getY()).isEqualTo(3);
    }

    @Test public void
    should_increase_mower_x_position() {
        Mower mower = new Mower(2,2,EAST);
        mower.setLimits(5,5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(3);
        assertThat(mower.getPosition().getY()).isEqualTo(2);
    }

    @Test public void
    should_decrease_mower_y_position() {
        Mower mower = new Mower(2,2,SOUTH);
        mower.setLimits(5,5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(2);
        assertThat(mower.getPosition().getY()).isEqualTo(1);
    }

    @Test public void
    should_decrease_mower_x_position() {
        Mower mower = new Mower(2,2,WEST);
        mower.setLimits(5,5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(2);
    }

    @Test public void
    should_not_move_north() {
        Mower mower = new Mower(5,5,NORTH);
        mower.setLimits(5, 5);
        mower.move();
        assertThat(mower.getPosition().getY()).isEqualTo(5);
    }

    @Test public void
    should_not_move_east() {
        Mower mower = new Mower(5,5,EAST);
        mower.setLimits(5, 5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(5);
    }

    @Test public void
    should_not_move_south() {
        Mower mower = new Mower(0,0,SOUTH);
        mower.setLimits(5, 5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(0);
    }

    @Test public void
    should_not_move_west() {
        Mower mower = new Mower(0,0,WEST);
        mower.setLimits(5, 5);
        mower.move();
        assertThat(mower.getPosition().getX()).isEqualTo(0);
    }

}