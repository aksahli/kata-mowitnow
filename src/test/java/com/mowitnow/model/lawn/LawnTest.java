package com.mowitnow.model.lawn;

import com.mowitnow.model.mower.Mower;
import org.junit.Test;

import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.assertj.core.api.Assertions.assertThat;

public class LawnTest {

    @Test public void
    should_throw_illegal_arguments_exception_when_width_negative() {
        try {
            new Lawn(-1,0);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Illegal lawn size [w:-1,h:0]");
        }
    }

    @Test public void
    should_throw_illegal_argument_exception_when_height_negative() {
        try {
            new Lawn(0,-1);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Illegal lawn size [w:0,h:-1]");
        }
    }

    @Test public void
    should_create_a_lawn() {
        Lawn lawn = new Lawn(5,5);

        assertThat(lawn).isNotNull();
        assertThat(lawn.getWidth()).isEqualTo(5);
        assertThat(lawn.getHeight()).isEqualTo(5);
        assertThat(lawn.getMowers())
            .isNotNull()
            .isEmpty();
    }

    @Test public void
    should_add_mower_to_the_lawn() {
        Lawn lawn = new Lawn(5,5);
        lawn.addMower(new Mower())
                .addMower(new Mower())
                .addMower(new Mower());

        assertThat(lawn.getMowers()).hasSize(3);
    }
}