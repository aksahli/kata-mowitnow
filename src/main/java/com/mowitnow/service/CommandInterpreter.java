package com.mowitnow.service;

import com.mowitnow.command.Command;
import com.mowitnow.command.MoveCommand;
import com.mowitnow.command.RotateCommand;
import com.mowitnow.model.action.Rotate;
import com.mowitnow.model.coordinate.Direction;
import com.mowitnow.model.lawn.Lawn;
import com.mowitnow.model.mower.Mower;

import java.util.ArrayList;
import java.util.List;

public class CommandInterpreter {

    /**
     *
     * create a lawn from lawn configuration
     *
     * @param lawnInput
     * @return
     */
    public Lawn getLawn(String lawnInput) {
        if(lawnInput == null || !lawnInput.matches("^[0-9] [0-9]")) {
            throw new IllegalArgumentException("Invalid lawn data");
        }
        String[] lawnData = lawnInput.split(" ");
        return new Lawn(Integer.valueOf(lawnData[0]),Integer.valueOf(lawnData[1]));
    }

    public Mower getMower(String mowerInput) {
        if(mowerInput == null || !mowerInput.matches("^[0-9] [0-9] (NORTH|SOUTH|WEST|EAST)$")) {
            throw new IllegalArgumentException("Invalid mower data");
        }
        String[] positionInput = mowerInput.split(" ");
        return new Mower(
                Integer.valueOf(positionInput[0]),
                Integer.valueOf(positionInput[1]),
                Direction.valueOf(positionInput[2])
        );
    }

    public List<Command> getCommand(String commandInput) {
        if(commandInput == null || !commandInput.matches("^(A|G|D)*$")) {
            throw new IllegalArgumentException("Invalid command input");
        }
        List<Command> commands =new ArrayList<>();
        for(int it=0;it<commandInput.length();it++) {
            if(commandInput.charAt(it) == 'A') {
                commands.add(new MoveCommand());
            }
            if(commandInput.charAt(it) == 'D') {
                commands.add(new RotateCommand(Rotate.RIGHT));
            }
            if(commandInput.charAt(it) == 'G') {
                commands.add(new RotateCommand(Rotate.LEFT));
            }
        }
        return commands;
    }


}
