package com.mowitnow.service;


import com.mowitnow.command.Command;
import com.mowitnow.model.lawn.Lawn;
import com.mowitnow.model.mower.Mower;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CommandExecutor {

    private CommandInterpreter interpreter;

    public void execute(Lawn lawn,List<String> input) throws IOException {

        lawn = interpreter.getLawn(input.get(0));

        Iterator<String> mowerInput = input.stream().skip(1).iterator();

        List<Command> allCommands = new ArrayList<>();
        while (mowerInput.hasNext()) {

            Mower mower = interpreter.getMower(mowerInput.next());
            lawn.addMower(mower);

            List<Command> commands = interpreter.getCommand(mowerInput.next());
            commands.stream().forEach(c -> c.setMower(mower));
            allCommands.addAll(commands);

        }

        allCommands.stream().forEach(c -> c.execute());

    }

    public void setInterpreter(CommandInterpreter interpreter) {
        this.interpreter = interpreter;
    }

}
