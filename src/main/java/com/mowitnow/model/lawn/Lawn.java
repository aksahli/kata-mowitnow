package com.mowitnow.model.lawn;

import com.mowitnow.model.mower.Mower;

import java.util.ArrayList;
import java.util.List;

public final class Lawn {

    private final int width, height;
    private List<Mower> mowers;

    /**
     *
     * Creates a new lawn
     *
     * @param width
     *            the lawn width
     * @param height
     *            the lawn height
     *
     */
    public Lawn(int width, int height) {
        if(width * height <= 0) {
            throw new IllegalArgumentException("Illegal lawn size [w:"+width+",h:"+height+"]");
        }
        this.width  = width;
        this.height = height;
        this.mowers = new ArrayList<>();
    }

    /**
     *
     * gets the width of the lawn
     *
     * @return width
     *
     */
    public int getWidth() {
        return this.width;
    }


    /**
     *
     * gets the height of the lawn
     *
     * @return height
     *
     */
    public int getHeight() {
        return  this.height;
    }

    /**
     *
     * gets the mowers list
     *
     * @return mowers
     *
     */
    public List<Mower> getMowers() {
        return mowers;
    }

    /**
     *
     * adds a mower to the lawn
     *
     * @param mower
     *          the mower to be added to the lawn
     *
     * @return the current lawn
     *
     */
    public Lawn addMower(Mower mower) {
        this.mowers.add(mower);
        mower.setLimits(this.width,this.height);
        return this;
    }


}
