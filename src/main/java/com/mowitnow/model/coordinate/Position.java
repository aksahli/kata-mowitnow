package com.mowitnow.model.coordinate;

public class Position {

    private int x;
    private int y;

    /**
     *
     * create a position with default values
     *
     */
    public Position() {
        this.x = 0;
        this.y = 0;
    }

    /**
     *
     * create a position with the specified values
     *
     * @param x
     *          the target x position
     * @param y
     *          the target y position
     *
     */
    public Position(int x,int y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * gets the x position
     *
     * @return x
     *
     */
    public int getX() {
        return this.x;
    }

    /**
     *
     * sets the x position
     *
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     *
     * gets the y position
     *
     * @return y
     *
     */
    public int getY() {
        return this.y;
    }

    /**
     *
     * sets the y position
     *
     */
    public void setY(int y) {
        this.y = y;
    }

}
