package com.mowitnow.model.coordinate;

public enum Direction {

    NORTH ( "EAST"  , "WEST"  ),
    SOUTH ( "WEST"  , "EAST"  ),
    EAST  ( "SOUTH" , "NORTH" ),
    WEST  ( "NORTH" , "SOUTH" );

    private String nextRight, nextLeft;

    Direction(String nextRight,String nextLeft) {
        this.nextRight = nextRight;
        this.nextLeft  = nextLeft;
    }

    public Direction rotateRight() {
        return valueOf(this.nextRight);
    }

    public Direction rotateLeft() {
        return valueOf(this.nextLeft);
    }
}
