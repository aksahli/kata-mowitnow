package com.mowitnow.model.action;

public enum Rotate {
    RIGHT('D'),LEFT('G');

    private char code;

    Rotate(char code) {
        this.code = code;
    }


    public Rotate valueOf(char code) {
        Rotate rotate = null;
        switch(code) {
            case 'D':
                rotate = Rotate.RIGHT;
                break;
            case 'G':
                rotate = Rotate.LEFT;
                break;
            default:
                break;
        }
        return rotate;
    }

}
