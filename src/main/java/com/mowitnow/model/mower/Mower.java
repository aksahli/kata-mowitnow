package com.mowitnow.model.mower;

import com.mowitnow.model.action.Rotate;
import com.mowitnow.model.coordinate.Direction;
import com.mowitnow.model.coordinate.Position;
import org.apache.log4j.Logger;

import java.util.Date;

import static com.mowitnow.model.action.Rotate.*;
import static com.mowitnow.model.coordinate.Direction.*;


public final class Mower {

    private static Logger logger = Logger.getLogger(Mower.class);

    private final Long      id;
    private final Position  position;
    private       Direction direction;
    private       int       limitY;
    private       int       limitX;

    /**
     *
     * Creates a new mower
     *
     */
    public Mower() {
        this.id        = new Date().getTime();
        this.position  = new Position();
        this.direction = NORTH;
    }

    /**
     *
     * Creates a new mower
     *
     * @param x
     *          the mower x position
     *
     * @param y
     *          the mower y position
     *
     * @param direction
     *          the mower direction
     *
     */
    public Mower(int x,int y,Direction direction) {
        this.id        = new Date().getTime();
        this.position  = new Position(x,y);
        this.direction = direction;
    }

    /**
     *
     * rotate the mowner
     *
     * @param rotation
     *          the rotation direction
     *
     * @return the current mowner
     */
    public final Mower rotate(Rotate rotation) {
        if(rotation == RIGHT) {
            this.direction = this.direction.rotateRight();
        } else if(rotation == LEFT) {
            this.direction = this.direction.rotateLeft();
        }
        return this;
    }

    /**
     *
     * move the mower forward
     *
     * @return the current mower
     *
     */
    public final Mower move() {
        if(this.direction == NORTH) {
            if(this.position.getY() < this.limitY) {
                this.position.setY(this.position.getY()+1);
            } else {
                logger.debug(this.id + " reached the north limit " + this.limitY);
            }
        }
        if(this.direction == EAST && this.position.getX() < this.limitX) {
            this.position.setX(this.position.getX() + 1);
        }
        if(this.direction == SOUTH && this.position.getY() > 0) {
            this.position.setY(this.position.getY()-1);
        }
        if(this.direction == WEST && this.position.getX() > 0) {
            this.position.setX(this.position.getX()-1);
        }
        return this;
    }

    /**
     *
     * gets the mower id;
     *
     * @return
     */
    public Long getId() {
        return id;
    }


    /**
     *
     * get the mower position
     *
     * @return position
     *
     */
    public Position getPosition() {
        return position;
    }

    /**
     *
     * get the mower direction
     *
     * @return direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     *
     * sets the upper limits of the mower movement
     *
     * @param limitY
     *
     */
    public void setLimits(int limitX,int limitY) {
        this.limitX = limitX;
        this.limitY = limitY;
    }


    @Override
    public String toString() {
        return new StringBuilder()
                .append(this.id)
                .append(" : [ ")
                .append(this.getPosition().getX())
                .append(" , ")
                .append(this.getPosition().getY())
                .append(" , ")
                .append(this.getDirection())
                .append(" ]")
                .toString();
    }

}
