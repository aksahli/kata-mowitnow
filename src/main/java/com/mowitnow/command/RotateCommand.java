package com.mowitnow.command;

import com.mowitnow.model.action.Rotate;
import com.mowitnow.model.mower.Mower;
import org.apache.log4j.Logger;

/**
 * mower-manager
 * com.mowitnow.command
 * <p>
 * Created by khalil on 22/11/15.
 */

public class RotateCommand implements Command {

    private static Logger logger = Logger.getLogger(RotateCommand.class);


    private Mower  mower;
    private Rotate rotate;

    /**
     *
     * creates a mower command
     *
     * @param rotate
     *          the rotation direction
     *
     */
    public RotateCommand(Rotate rotate) {
        this.rotate = rotate;
    }

    /**
     *
     * creates a mower command
     *
     * @param mower
     *          the mower concerned by the command
     *
     * @param rotate
     *          the rotation direction
     *
     */
    public RotateCommand(Mower mower, Rotate rotate) {
        this.mower  = mower;
        this.rotate = rotate;
    }

    @Override
    public void setMower(Mower mower) {
        this.mower = mower;
    }

    @Override
    public void execute() {
        logger.debug("rotate to "+ rotate +" : " + this.mower);
        this.mower.rotate(this.rotate);
    }
}
