package com.mowitnow.command;

import com.mowitnow.model.mower.Mower;
import org.apache.log4j.Logger;


public class MoveCommand implements Command {

    private static Logger logger = Logger.getLogger(MoveCommand.class);

    private Mower mower;

    /**
     *
     * creates a mower command
     *
     */
    public MoveCommand() {}

    /**
     *
     * creates a mower command
     *
     * @param mower
     *          the mower concerned by the command
     *
     */
    public MoveCommand(Mower mower) {
        this.mower = mower;
    }

    @Override
    public void setMower(Mower mower) {
        this.mower = mower;
    }

    @Override
    public void execute() {
        logger.debug("move to "+ this.mower.getDirection() +" : "+this.mower);
        this.mower.move();
    }

}
