package com.mowitnow.command;

import com.mowitnow.model.mower.Mower;

public interface Command {

    /**
     *
     * execute the command
     *
     */
    void execute();

    /**
     *
     * sets the mower concerned by the command
     *
     * @param mower
     *          the mower concerned by the command
     *
     */
    void setMower(Mower mower);
}
